if ! contains ~/.cargo/bin $PATH
  set -a PATH ~/.cargo/bin
end
